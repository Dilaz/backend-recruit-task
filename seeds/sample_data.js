const {USER_TABLE, TODO_TABLE, TODOLIST_TABLE} = require('./../constants/tables');

const DEFAULT_LIST = {
  title: 'Default',
};

const TODO_LIST = [
  {
    title: "Buy bread"
  }, {
    title: "Pay rent"
  }, {
    title: "Take the dog for a walk"
  }
];

const USER_LIST = [
  {
    username: "pentti",
    name: "Pentti",
    lastName: "Placeholder",
    password: "$2y$10$bthiqn0Fb/Fu43FqKTF1beEVPf7/ImgfIi5U8j.gZdnytx5LiILiu" // hunter2
  }, {
    username: "milla",
    name: "Milla",
    lastName: "Mallikas",
    password: "$2y$10$j2.JXGGDUDyUQi/qMVwEAOU9kiuMSOrO7F.eexV3wxSMX6j1RnEze", // yksisarvinen5
  }, {
    username: "kaija",
    name: "Kaija",
    lastName: "Koodari",
    password: "$2y$10$MOzAsaIbqdWGjy49b5uQXeTIufYJydQF9HfGDH6CRQFv8/qlf6bDa", // PCGamerMasterrace
  }
];

exports.seed = async function (knex) {
  await knex(USER_TABLE).insert(USER_LIST);
  const users = await knex(USER_TABLE).select('*');

  const promises = users.map(async user => {
    const todoListId = await knex(TODOLIST_TABLE).insert({...DEFAULT_LIST, userId: user.id});
    const userTodos = TODO_LIST.map(baseItem => {
      const userTodoDetails = {
        listId: todoListId[0],
        title: baseItem.title,
        description: `${user.name} remember: ${baseItem.title}`
      };
      return Object.assign(userTodoDetails, baseItem)
    });
    return await knex(TODO_TABLE).insert(userTodos)
  });
  await Promise.all(promises);
  console.log('seed done')
};
