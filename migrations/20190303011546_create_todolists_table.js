const { tables } = require('../constants');

exports.up = knex => knex.schema
  .createTable(tables.TODOLIST_TABLE, t => {
    t.increments('id').primary();
    t.string('title');

    t.bigInteger('createdAt').notNullable().defaultTo(knex.fn.now());
    t.bigInteger('updatedAt').notNullable().defaultTo(knex.fn.now());

    t
      .integer('userId')
      .unsigned()
      .references('id')
      .inTable(tables.USER_TABLE)
      .onDelete('CASCADE');
  });

exports.down = knex => knex.schema
  .dropTableIfExists(tables.USER_TABLE);
