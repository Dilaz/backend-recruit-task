const { tables } = require('../constants');

exports.up = knex => {
  return knex.schema
    .createTable(tables.USER_TABLE, table => {
      table.increments('id').primary();
      table.string('username').unique().notNullable();
      table.string('name');
      table.string('lastName');

      table.bigInteger('createdAt').notNullable().defaultTo(knex.fn.now());
      table.bigInteger('updatedAt').notNullable().defaultTo(knex.fn.now());
    });
};

exports.down = knex => {
  return knex.schema
    .dropTableIfExists(tables.USER_TABLE);
};
