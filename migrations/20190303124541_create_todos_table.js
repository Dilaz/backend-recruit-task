const { tables } = require('../constants');

exports.up = knex => {
  return knex.schema
    .createTable(tables.TODO_TABLE, table => {
      table.increments('id').primary();
      table.string('title').notNullable();
      table.string('description');

      table.bigInteger('createdAt').notNullable().defaultTo(knex.fn.now());
      table.bigInteger('updatedAt').notNullable().defaultTo(knex.fn.now());

      table
        .integer('listId')
        .unsigned()
        .references('id')
        .inTable(tables.TODOLIST_TABLE)
        .onDelete('CASCADE');
    });
};

exports.down = knex => {
  return knex.schema
    .dropTableIfExists(tables.TODO_TABLE);
};
