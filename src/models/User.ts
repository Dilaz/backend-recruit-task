import { Model } from 'objection';
import { tables } from '../../constants';
import { hashPassword } from '../accounts/password';
import _ = require('lodash');
import Todo from './Todo';

export default class User extends Model {
  static tableName = tables.USER_TABLE;
  static jsonSchema = {
    type: 'object',
    required: ['username', 'password'],

    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 1, maxLength: 255 },
    },
  };
  readonly id! : number;
  username : string;
  name? : string;
  lastName? : string;
  password : string;
  todos? : Todo[];

  static relationMappings = {
    lists: {
      relation: Model.HasManyRelation,
      modelClass: 'TodoList',
      join: {
        from: `${tables.USER_TABLE}.id`,
        to: 'todolists.userId',
      },
    },
    todos: {
      relation: Model.ManyToManyRelation,
      modelClass: 'Todo',
      join: {
        from: `${tables.USER_TABLE}.id`,
        through: {
          from: `${tables.TODOLIST_TABLE}.userId`,
          to: `${tables.TODOLIST_TABLE}.id`,
        },
        to: `${tables.TODO_TABLE}.listId`,
      },
    },
  };

  static modelPaths = [__dirname];

  /**
   * beforeInsert filter to hash the password before saving it to the DB. This should also be done in $beforeUpdate,
   * but since there is no such functionality, I'll just leave it out
   * @param ctx
   */
  async $beforeInsert(ctx) {
    const superPromise = super.$beforeInsert(ctx);
    return Promise.resolve(superPromise)
    .then(() => hashPassword(this.password))
    .then(passObject => this.password = passObject.hash);
  }

  /**
   * JSON filter to hide password field
   * @param json
   */
  $formatJson(json) {
    const jsonFields = super.$formatJson(json);
    return _.omit(jsonFields, ['password']);
  }
}
