import { Model } from 'objection';
import { tables } from '../../constants';

export default class TodoList extends Model {
  static tableName = tables.TODOLIST_TABLE;
  static relationMappings = {
    user: {
      relation: Model.BelongsToOneRelation,
      modelClass: 'User',
      join: {
        from: `${tables.TODOLIST_TABLE}.userId`,
        to: `${tables.USER_TABLE}.id`,
      },
    },
    todos: {
      relation: Model.HasManyRelation,
      modelClass: 'Todo',
      join: {
        from: `${tables.TODOLIST_TABLE}.id`,
        to: `${tables.TODO_TABLE}.listId`,
      },
    },
  };

  static modelPaths = [__dirname];

  static jsonSchema = {
    type: 'object',
    required: ['title'],

    properties: {
      id: { type: 'integer' },
      title: { type: 'string', minLength: 1, maxLength: 255 },
      userId: { type: ['integer', null] },
    },
  };
  readonly id! : number;
  title : string;
  userId : number;
}
