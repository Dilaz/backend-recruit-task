import { Model } from 'objection';
import { tables } from '../../constants';

export class Todo extends Model {
  static tableName = tables.TODO_TABLE;

  readonly id!: number;
  title: string;
  description?: string;
  listId: number;

  static relationMappings = {
    list: {
      relation: Model.BelongsToOneRelation,
      modelClass: 'TodoList',
      join: {
        from: `${tables.TODO_TABLE}.listId`,
        to: `${tables.TODOLIST_TABLE}.id`,
      },
    },
    user: {
      relation: Model.HasOneThroughRelation,
      modelClass: 'User',
      join: {
        from: `${tables.TODO_TABLE}.listId`,
        through: {
          from: `${tables.TODOLIST_TABLE}.id`,
          to: `${tables.TODOLIST_TABLE}.userId`,
        },
        to: `${tables.USER_TABLE}.id`,
      },
    },
  };

  static modelPaths = [__dirname];

  static jsonSchema = {
    type: 'object',
    required: ['title'],

    properties: {
      id: { type: 'integer' },
      title: { type: 'string', minLength: 1, maxLength: 255 },
      description: { type: 'string', minLength: 1, maxLength: 255 },
      listId: { type: ['integer', null] },
    },
  };
}

export default Todo;
