import * as express from 'express';
import TodoController from './controllers/TodoController';
import AuthController from './controllers/AuthController';
import UserController from './controllers/UserController';
import ListController from './controllers/ListController';


export default (router: express.Router) => {
  // Init list of controllers
  const controllers = [
    new TodoController(router),
    new ListController(router),
    new AuthController(router),
    new UserController(router),
  ];

  // Add each controller routes to router
  controllers.forEach((controller) => {
    router.use('/', controller.router);
  });
};
