import User from '../models/User';
import Controller from './Controller';

export default class UserController extends Controller {
  /**
   * @extends Controller::initRoutes
   */
  public initRoutes() {
    // Since I'm not sure what this route is for, I leave it as it is. In real-life scenario, this kind of route
    // probably would be available only for admin users. At least the password hash is hidden from this json dump
    this.router.get('/users', async (req, res) => {
      const users : User[] = await User.query();
      res.send(users);
    });
  }
  /**
   * @extends Controller::initMiddleware
   */
  initMiddleware() {
  }
}
