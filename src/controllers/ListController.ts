import Controller from './Controller';
import { BadRequestError, NotFoundError } from '../Errors';
import { validateToken } from '../accounts/jwt';
import User from '../models/User';
import TodoList from '../models/TodoList';
import Todo from '../models/Todo';

export default class ListController extends Controller {
  /**
   * @extends Controller::initMiddleware
   */
  public initMiddleware() {
    this.router.all('/lists*', this.checkToken);
  }

  /**
   * @extends Controller::initRoutes
   */
  initRoutes() : void {
    this.router.post('/lists', this.addList);
    this.router.get('/lists', this.getLists);
    this.router.get('/lists/:id', this.getSingleList);
    this.router.delete('/lists/:id', this.deleteList);
  }

  /**
   * Get all todos for the current user
   * @param req express.Request
   * @param res express.Response
   */
  public async getLists(req, res) {
    const user : User = req.user;
    const lists : TodoList[] = await user.$relatedQuery<TodoList>('lists');
    res.send(lists);
  }

  /**
   * Get a single list by ID
   * @param req express.Request
   * @param res express.Response
   */
  public async getSingleList(req, res) {
    const id : number = req.params.id;

    // Get the user from the token
    const user : User = req.user;

    // Get the list by ID and eager-load the todos
    const list : TodoList = await user.$relatedQuery<TodoList>('lists').findById(id).eager('todos');
    if (!list) throw new NotFoundError('No such List!');

    res.send(list);
  }

  /**
   * Add a new todo for the user
   * @param req express.Request
   * @param res express.Response
   */
  public async addList(req, res) {
    const user : User = req.user;
    const title : string = req.body.title;
    const todos : Todo[] = req.body.todos;

    const list : TodoList = await user.$relatedQuery<TodoList>('lists').insertAndFetch({ title });
    if (!list) throw new BadRequestError('Could not create a new list');

    // Add todos to the list if they exist
    if (todos) {
      await Promise.all(todos.map(async todo => await list.$relatedQuery<Todo>('todos').insert(todo)));
    }

    res.send({ status: 'ok', listId: list.id });
  }

  /**
   * Delete a todo by id
   * @param req express.Request
   * @param res express.Response
   */
  public async deleteList(req, res) {
    const user : User = req.user;
    const listId : number = req.params.id;

    await user.$relatedQuery<TodoList>('lists').where({ id: listId }).delete();

    res.send({ status: 'ok' });
  }

  /**
   *  Middleware to check the token provided and save the user
   * @param req express.Request
   * @param res express.Response
   * @param next
   */
  private async checkToken(req, res, next) {
    const token : string = req.query.token;
    if (!token) throw new BadRequestError('Invalid token!');

    // Verify the token
    let parsedToken : any = null;
    try {
      parsedToken = validateToken(token);
    } catch (err) {
      throw new BadRequestError('Invalid token!');
    }
    req.user = await User.query().findById(parsedToken.sub);

    if (!req.user) throw new BadRequestError('Invalid user!');
    next();
  }
}
