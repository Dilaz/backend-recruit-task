import * as express from 'express';

/**
 * Base class for controllers
 */
export default abstract class Controller {
  public router : express.Router = null;

  /**
   * Constructor
   * @param router express.Router
   */
  constructor(router: express.Router) {
    this.router = router;
    this.initMiddleware();
    this.initRoutes();
  }

  /**
   * Method to init the middleware routes
   */
  abstract initMiddleware() : void;

  /**
   * Method to init the actual routes
   */
  abstract initRoutes() : void;
}
