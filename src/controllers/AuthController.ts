// Auth routes
import { BadRequestError, NotFoundError } from '../Errors';
import User from '../models/User';
import { comparePassword } from '../accounts/password';
import { issueToken } from '../accounts/jwt';
import Controller from './Controller';
import { ValidationError } from 'objection';

export default class AuthController extends Controller {
  /**
   * @extends Controller::initRoutes
   */
  public initRoutes() {
    this.router.post('/login', this.login);
    this.router.post('/register', this.register);
  }

  /**
   * Validates user login and returns a JWT token
   * @param req express.Request
   * @param res express.Response
   */
  public async login(req, res) {
    const username = req.body.username;
    const password = req.body.password;

    if (!username || !password) throw new BadRequestError('Username or password missing!');

    const user : User = await User.query().where({ username }).first();
    if (!user) throw new NotFoundError('User not found!');

    if (!await comparePassword(password, user.password)) throw new BadRequestError('Password validation failed');

    // Issue token with default validation time for the current user
    res.send(issueToken(user.id));
  }

  /**
   * Registers a new user
   * @param req express.Request
   * @param res express.Response
   */
  public async register(req, res) {
    const { username, name, lastName, password } = req.body;

    // Make sure username is not already in use
    const userExists : User = await User.query().where({ username }).first();
    if (userExists) {
      throw new BadRequestError(`User "${username}" already exists`);
    }

    // Create a new user
    const user : User = await User.query().insertAndFetch({ username, name, lastName, password });

    // Everything went better than expected
    res.send({ status: 'ok', userId: user.id });
  }


  /**
   * @extends Controller::initMiddleware
   */
  initMiddleware() {
  }
}
