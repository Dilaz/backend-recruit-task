import { BadRequestError, NotFoundError } from '../Errors';
import { validateToken } from '../accounts/jwt';
import Todo from '../models/Todo';
import User from '../models/User';
import Controller from './Controller';
import TodoList from '../models/TodoList';

export default class TodoController extends Controller {
  /**
   * @extends Controller::initMiddleware
   */
  public initMiddleware() {
    this.router.all('/todos*', this.checkToken);
  }

  /**
   * @extends Controller::initRoutes
   */
  public initRoutes() {
    this.router.post('/todos', this.addTodo);
    this.router.get('/todos', this.getTodos);
    this.router.get('/todos/:id', this.getSingleTodo);
    this.router.delete('/todos/:id', this.deleteTodo);
  }

  /**
   * Get all todos for the current user
   * @param req express.Request
   * @param res express.Response
   */
  public async getTodos(req, res) {
    const user : User = req.user;

    const todos = await user.$relatedQuery<Todo>('todos');

    res.send(todos);
  }

  /**
   * Get a single todo by ID (only for current user)
   * @param req express.Request
   * @param res express.Response
   */
  public async getSingleTodo(req, res) {
    const id : number = req.params.id;

    // Get the user from the token
    const user : User = req.user;

    const todo =  await user.$relatedQuery<Todo>('todos').findById(id);
    if (!todo) throw new NotFoundError('No such Todo!');

    res.send(todo);
  }

  /**
   * Add a new todo for the user
   * @param req express.Request
   * @param res express.Response
   */
  public async addTodo(req, res) {
    const user : User = req.user;
    const listId : number = req.body.listId;
    const title : string = req.body.title;
    const description : string = req.body.description;

    // Get the todo list
    const list = await user.$relatedQuery<TodoList>('lists').findById(listId);
    if (!list) throw new BadRequestError('List not found');

    const todo : Todo = await list.$relatedQuery<Todo>('todos').insertAndFetch({ title, description });

    res.send({ status: 'ok', todoId: todo.id });
  }

  /**
   * Delete a todo by id
   * @param req express.Request
   * @param res express.Response
   */
  public async deleteTodo(req, res) {
    const user : User = req.user;
    const todoId : number = req.params.id;

    await user.$relatedQuery<Todo>('todos').where({ id: todoId }).delete();

    res.send({ status: 'ok' });
  }

  /**
   *  Middleware to check the token provided and save the user
   * @param req express.Request
   * @param res express.Response
   * @param next
   */
  private async checkToken(req, res, next) {
    const token : string = req.query.token;
    if (!token) throw new BadRequestError('Invalid token!');

    // Verify the token
    let parsedToken : any = null;
    try {
      parsedToken = validateToken(token);
    } catch (err) {
      throw new BadRequestError('Invalid token!');
    }
    req.user = await User.query().where({ id: parsedToken.sub }).first();

    if (!req.user) throw new BadRequestError('Invalid user!');

    next();
  }

}

